# ska-mid-spfrx-talondx

This repository is intended for uploading the Mid.DSH SPFRx TALON-DX's FPGA bitstream 
raw packages to the [Central Artefact Repository](https://artefact.skao.int/). 
It is based on the [ska-mid-cbf-talondx](https://gitlab.com/ska-telescope/ska-mid-cbf-talondx), and ultimately the
[ska-raw-skeleton](https://gitlab.com/ska-telescope/templates/ska-raw-skeleton) repository.

## Initialization

### Prerequisites
- Git
- [Git Large File Storage:](https://git-lfs.github.com/) 
    ```bash
    apt install git-lfs
    ```

Clone the repository locally, then set up the `.make` submodule and `git-lfs`.
```bash
git submodule init
git submodule update
git lfs install
```
*Note: `git lfs install` need only be run once per user.*

## Release FPGA bitstream raw packages
1. On branch `main`, add the bitstream files to `raw/ska-mid-spfrx-talondx/`. Each sub-directory of `raw/` prefixed with `ska-` will be packaged and uploaded to the CAR separately upon tag pipeline success, so delete any directories not intended for release; currently, only the `ska-mid-spfrx-talondx` package is used.

2. Update the `.release` file version; if simply incrementing the current 
semver, can use the following make rules: `make bump-patch-release`, 
`make bump-minor-release` or `make bump-major-release`.

    *Note: currently the `.make` submodule applies the same semver tag (from `.release`) to all packages created from the sub-directories of `raw/`.*

3. Commit changes and tag release. Ensure that LFS pointers are staged properly for commit with `git lfs status`;
```bash
git add .
git lfs status
```
Output should say `LFS` next to files in parentheses:
```bash
...
Git LFS objects to be committed:

        raw/ska-mid-spfrx-talondx/bin/...rbf (LFS: ...)
        raw/ska-mid-spfrx-talondx/bin/...jic (LFS: ...)
...
```
To commit, tag and push both in fewer commands:
```bash
git commit -m <message>
git tag <tag>
git push --atomic origin main <tag>
```